"""
lorenz.py

UVA CS 1120 -- Problem Set 4 -- Constructing Colossi
"""

# We store the Baudot codes in a Python dictionary (see cs101 Lesson 5):

BAUDOT_CODES = {
    'A': [0,0,0,1,1], 'B': [1,1,0,0,1], 'C': [0,1,1,1,0], 'D': [0,1,0,0,1],
    'E': [0,0,0,0,1], 'F': [0,1,1,0,1], 'G': [1,1,0,1,0], 'H': [1,0,1,0,0],
    'I': [0,0,1,1,0], 'J': [0,1,0,1,1], 'K': [0,1,1,1,1], 'L': [1,0,0,1,0],
    'M': [1,1,1,0,0], 'N': [0,1,1,0,0], 'O': [1,1,0,0,0], 'P': [1,0,1,1,0],
    'Q': [1,0,1,1,1], 'R': [0,1,0,1,0], 'S': [0,0,1,0,1], 'T': [1,0,0,0,0],
    'U': [0,0,1,1,1], 'V': [1,1,1,1,0], 'W': [1,0,0,1,1], 'X': [1,1,1,0,1],
    'Y': [1,0,1,0,1], 'Z': [1,0,0,0,1], ' ': [0,0,1,0,0], ',': [0,1,0,0,0],
    '-': [0,0,0,1,0], '.': [1,1,1,1,1], '!': [1,1,0,1,1]}

# This is a condensed way to produce the inverted mapping.
# (We need to convert the mutable lists to immutable tuples to
#  use them as dictionary keys. (It would be more sensible to use
#  tuples throughout, but want to keep things simple with lists.)

INVERTED_BAUDOT_CODES = { tuple(v): k for k, v in BAUDOT_CODES.items() }

def char_to_baudot(c):
    """
    Returns the five-bit Baudot sequence corresponding to the input character as
    a list of numbers (or [0,0,0,0,0] if there is no code for the input).
    """
    if c in BAUDOT_CODES:
        return BAUDOT_CODES[c]
    else:
        return [0,0,0,0,0]

# Function to convert a character's Baudot character to the actual
# character as a string
def baudot_to_char(code):
    """
    Returns the character (single-character string) corresponding to the input Baudot code
    (or * if there is no matching code).
    """
    if tuple(code) in INVERTED_BAUDOT_CODES:
        return INVERTED_BAUDOT_CODES[tuple(code)]
    else:
        return "*"

def test_conversions():
    for c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ,-.!':
        assert len(char_to_baudot(c)) == 5
        assert baudot_to_char(char_to_baudot(c)) == c
    print("Passed all tests!")
    
# Definition for the various wheels of our Lorenz Machine

K_wheels = [[1,1,0,1,0],[0,1,0,0,1],[1,0,0,1,0],[1,1,1,0,1],[1,0,0,0,1]]
S_wheels = [[0,0,0,1,1],[0,1,1,0,0],[0,0,1,0,1],[1,1,0,0,0],[1,0,1,1,0]]
M_wheel = [0,0,1,0,1]

# The first ciphertext, you will decrypt this in brute_force_lorentz:
CIPHERTEXT = "GR-!-EN .H.CVZQZMRRFKSF QV*L!LO-TP IY,PG.VUPNWKSNBVRUEW COXORUVJJNVD-SL*ZYCZVSU. MQTNLU PBEJ-FJVSF CXRUER KZOEGUUBLWKUVBW,MYEWM.RLST,OZIKKW*ZUUUUVSWWXQXH,IITOMQG VUUXA*JSGWSACW."


